package oop.asg02;

import java.util.Random;


/**
 * Hello world!
 *
 */
public class App
{
	static final int kNumberOfTest = 1000;
	static final int kMaxDigits = 10;
	static final boolean kNegative = true;
	static final boolean randomInput = true;
	
    public static void main( String[] args ) {    	
    	String s1, s2;
    	s1 = "-920";
    	s2 = "-920";

    	for(int i = 0; i < kNumberOfTest; i++) {
    		System.out.println("Test " + i + "th ------- ");    		
    		
    		if(randomInput) {
    			s1 = getRandom();
    			s2 = getRandom();
    		}
    		
    		System.out.println("s1: " + s1 + "\ns2: " + s2);
    		
    		BigInteger x = new BigInteger(s1);
    		BigInteger y = new BigInteger(s2);
    		java.math.BigInteger ansX = new java.math.BigInteger(s1);
    		java.math.BigInteger ansY = new java.math.BigInteger(s2);
    		    		    		
    		boolean ok = true;
    		ok &= x.equals(ansX);
    		System.out.println("Equal check X: " + ok);

    		ok &= y.equals(ansY); 
    		System.out.println("Equal check Y: " + ok);

            if(/*kNegative || */ ansX.compareTo(ansY) > 0) {
                ok &= x.subtract(y).equals(ansX.subtract(ansY));
                System.out.println("X - Y: " + ok);
                /*System.out.println(x.subtract(y));
                System.out.println(ansX.subtract(ansY));*/
            } else {
                ok &= y.subtract(x).equals(ansY.subtract(ansX));
                System.out.println("Y - X: " + ok);
                /*System.out.println(y.subtract(x));
                System.out.println(ansY.subtract(ansX));*/
            }

    		ok &= x.add(y).equals(ansX.add(ansY));
    		System.out.println("X + Y: " + ok);
    			   
    		if(!ok) {
    			System.err.println("Error!!!");
    			return;
    		}
    	}
    	
    	System.out.println("Done!!!");    	
    }
    

	private static String getRandom() {
		String result = "";
		Random rand = new Random();		
		int length = rand.nextInt(kMaxDigits)+1;
		for(int i = 0; i < length; i++) 			
			result += rand.nextInt(10);
		
		int sign = rand.nextInt(2);
		if(sign == 1 && kNegative)			
			result = '-' + result; 
		
		return result;
	}
    
}

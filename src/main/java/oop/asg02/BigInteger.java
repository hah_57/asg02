package oop.asg02;

import java.lang.Long;

/**
 * Created with IntelliJ IDEA.
 * User: halink
 * Date: 9/13/13
 * Time: 3:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class BigInteger {

    private String bigInteger;

    public BigInteger() {
    }

    public BigInteger(int p){
        bigInteger = "";
        if( p == 0 ) {
            bigInteger = "0";
        }
        while(p >0){
            this.bigInteger = p % 10 + bigInteger;
            p/=10;
        }
    }

    public BigInteger(long p){
        bigInteger = "";
        if(p == 0) {
            bigInteger = "0";
        }
        while(p >0){
            this.bigInteger = p % 10 + bigInteger;
            p/=10;
        }
    }

    public BigInteger daodau(BigInteger a){
        BigInteger c = new BigInteger("");
        int m = a.bigInteger.length();

        if(a.bigInteger.charAt(0) == '-'){
            for (int i=m-1; i>0; i--){
                c.bigInteger = a.bigInteger.charAt(i)+c.bigInteger;
            }
        } else {
            for (int i=m-1; i>=0; i--){
                c.bigInteger = a.bigInteger.charAt(i)+c.bigInteger;
            }
            c.bigInteger = '-' + c.bigInteger;
        }
        return c;
    }

    public BigInteger(String st){
        bigInteger = "";
        if(st.isEmpty()) return;
        int n=0;

        // check negative number
        boolean negative = false;
        if(st.charAt(n) == '-'){
            negative = true;
            n++;
        }

        // delete digit 0 at first character
        while (st.charAt(n) == '0' && n<st.length()-1) n++;

        // create new big integer
        int m= st.length();
        for(int i=m-1; i>=n; i--){
            bigInteger = st.charAt(i) +bigInteger;
        }

        if (bigInteger.compareTo("0") == 0) return;

        // if nagative number then add '-' in the first
        if(negative){
            bigInteger = '-'+bigInteger;
        }
    }

    @Override
    public String toString() {
        return bigInteger;
    }

    public BigInteger add(BigInteger b){

        if(this.bigInteger.charAt(0) ==  '-' && b.bigInteger.charAt(0) == '-'){
            BigInteger d = daodau(this);
            BigInteger e = daodau(b);
            return(daodau(d.add(e)));
        } else{
            if(this.bigInteger.charAt(0) == '-' && b.bigInteger.charAt(0)!='-'){
                BigInteger d = daodau(this);
                return b.substract(d);
            } else{
                if (this.bigInteger.charAt(0) != '-' && b.bigInteger.charAt(0) =='-'){
                    BigInteger e = daodau(b);
                    return this.substract(e);
                }
            }
        }

        // create big integer null
        BigInteger c = new BigInteger("");

        while(bigInteger.length() < b.bigInteger.length()) bigInteger = '0' + bigInteger;
        while(b.bigInteger.length() < bigInteger.length()) b.bigInteger = '0' + b.bigInteger;

        //add process
        int m = bigInteger.length();
        int nho =0;
        for(int i=m-1; i>=0; i--){
            int t = (b.bigInteger.charAt(i) + bigInteger.charAt(i)- 96 ) + nho;
            c.bigInteger = (t % 10) + c.bigInteger;
            if(t > 9){
                nho = 1;
            } else {
                nho =0;
            }
        }
        if(nho > 0) {
            c.bigInteger = 1 + c.bigInteger;
        }


        return c;
    }

    public BigInteger substract(BigInteger b){

        if(this.bigInteger.charAt(0) ==  '-' && b.bigInteger.charAt(0) == '-'){
            BigInteger d = daodau(this);
            BigInteger e = daodau(b);
            return e.substract(d);
        } else{
            if(this.bigInteger.charAt(0) == '-' && b.bigInteger.charAt(0)!='-'){
                BigInteger d = daodau(this);
                return daodau(d.add(b));
            } else{
                if (this.bigInteger.charAt(0) != '-' && b.bigInteger.charAt(0) =='-'){
                    BigInteger e = daodau(b);
                    return this.add(e);
                }
            }
        }


        BigInteger c = new BigInteger("");
        while(bigInteger.length() < b.bigInteger.length()) bigInteger = '0' + bigInteger;
        while(b.bigInteger.length() < bigInteger.length()) b.bigInteger = '0' + b.bigInteger;
        int m=bigInteger.length();

        boolean bigger = false;
        int hieu = 0;
        for(int i=0; i<m; i++){
            if(b.bigInteger.charAt(i) > this.bigInteger.charAt(i)) {
                bigger = true;
                break;
            }
            if(this.bigInteger.charAt(i) > b.bigInteger.charAt(i)) {
                break;
            }
        }
        if(bigger){
            BigInteger tg = new BigInteger(0);
            tg.bigInteger = this.bigInteger;
            this.bigInteger = b.bigInteger;
            b.bigInteger = tg.bigInteger;
            hieu = -1;
        }

        // substract processing
        int nho = 0;
        for(int i=m-1; i>=0; i--){
            int sub = this.bigInteger.charAt(i) - b.bigInteger.charAt(i) - nho;
            if(sub < 0){
                sub += 10;
                nho = 1;
            } else {
                nho = 0;
            }
            c.bigInteger = sub + c.bigInteger;
        }

        //delete digit 0 at first character
        if(c.bigInteger.charAt(0) == '0'){
            int n=0;
            while (c.bigInteger.charAt(n) == '0' && n<c.bigInteger.length()-1){
                n++;
            }
            m = c.bigInteger.length();
            BigInteger t = new BigInteger("");
            for(int i=m-1; i>=n; i--){
                t.bigInteger = c.bigInteger.charAt(i) + t.bigInteger;
            }
            c.bigInteger = t.bigInteger;
        }

        // add "-" character for number smaller than 0
        if(hieu == -1 ){
            c.bigInteger = '-' + c.bigInteger;
        }
        return  c;
    }

    public BigInteger subtract(BigInteger b){
        return substract(b);
    }

    /*public  boolean equals(Object other){
        BigInteger b = (BigInteger) other;

        if (bigInteger.length() != b.bigInteger.length()) {
            return false;
        }

        int m = bigInteger.length();
        for(int i=0; i<m; i++){
            if(this.bigInteger.charAt(i) != b.bigInteger.charAt(i)){
                return false;
            }
        }
        return true;
    }*/

    public boolean equals(Object other) {
        return toString().equals(other.toString());
    }


    public int compareTo(BigInteger a){
        if(bigInteger.charAt(0) == '-' && a.bigInteger.charAt(0) != '-') return -1;
        if(bigInteger.charAt(0) != '-' && a.bigInteger.charAt(0) == '-') return 1;
        if(bigInteger.length() > a.bigInteger.length()) {
            if(bigInteger.charAt(0) == '-'){
                return  -1;
            } else return 1;
        }
        if(bigInteger.length() < a.bigInteger.length()){
           if (bigInteger.charAt(0) == '-'){
               return  1;
           } else{
                return -1;
           }
        }

        int m=bigInteger.length();
        for (int i=0; i<m; i++){
            if(bigInteger.charAt(i) > a.bigInteger.charAt(i)){
                if(bigInteger.charAt(0) != '-'){
                    return 1;
                } else {
                    return -1;
                }
            } else if(bigInteger.charAt(i) < a.bigInteger.charAt(i)){
                if (bigInteger.charAt(0) != '-'){
                    return -1;
                }   else {
                    return 1;
                }
            }
        }

        return  0;
    }

    public BigInteger clone(){
        BigInteger d = new BigInteger(this.bigInteger);
        //BigInteger d = this;
        return d;
    }

    public  int toInt(){
        int a =0;
        int m = this.bigInteger.length();
        for(int i=0; i<m; i++){
            a= a*10 + this.bigInteger.charAt(i) - 48;
        }
        return a;
    }

    public  long toLong(){
        long a = 0;
        int m = this.bigInteger.length();
        for(int i=0; i<m; i++){
            a= a*10 + this.bigInteger.charAt(i) - 48;
        }
        return a;
    }
}

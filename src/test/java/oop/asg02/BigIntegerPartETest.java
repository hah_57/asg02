package oop.asg02;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Unit test for BigInteger class.
 */
public class BigIntegerPartETest
{
    @Test
    public void testClone() {
        BigInteger bigInt1 = new BigInteger("111112461111122111111");
        BigInteger bigInt2 = bigInt1.clone();
        assertNotSame(bigInt1, bigInt2);
        assertEquals(bigInt1, bigInt2);
    }

    @Test
    public void testClone2() {
        BigInteger bigInt1 = new BigInteger(1234567890);
        BigInteger bigInt2 = bigInt1.clone();
        assertNotSame(bigInt1, bigInt2);
        assertEquals(bigInt1, bigInt2);
    }

    @Test
    public void testCompareTo_DifferentLength_Bigger() {
        BigInteger bigInt1 = new BigInteger("1884638881");
        BigInteger bigInt2 = new BigInteger("16674657445636");

        assertEquals(1, bigInt2.compareTo(bigInt1));
    }

    @Test
    public void testCompareTo_DifferentLength_Smaller() {
        BigInteger bigInt1 = new BigInteger("1884638881");
        BigInteger bigInt2 = new BigInteger("16674657445636");

        assertEquals(-1, bigInt1.compareTo(bigInt2));
    }

    @Test
    public void testCompareTo_SameLength() {
        BigInteger bigInt1 = new BigInteger("1884638881");
        BigInteger bigInt2 = new BigInteger("4667465744");

        assertEquals(-1, bigInt1.compareTo(bigInt2));
        assertEquals(1, bigInt2.compareTo(bigInt1));
    }

    @Test
    public void testCompareTo_Equal() {
        BigInteger bigInt1 = new BigInteger("12345678901234567890");
        BigInteger bigInt2 = new BigInteger("12345678901234567890");

        assertEquals(0, bigInt1.compareTo(bigInt2));
        assertEquals(0, bigInt2.compareTo(bigInt1));
    }
}